using Matrices;
using Microsoft.VisualStudio.CodeCoverage;
using NUnit.Allure.Attributes;
using NavalCombat;
using System.Drawing;
using System.Linq.Expressions;
using System.Reflection;

namespace NavalCombat.Tests;


[TestFixture(TestOf = typeof(Game), Description = "Game tests")]
public class GameTest
{
    [Test]
    public void PutShipsTest()
    {
        Game game = new();

        var ship1 = game.MyField.PutShip("�1", "�1");
        CollectionAssert.AreEqual(ship1.Decks, new Address[] { "�1", "�1", "�1", "�1" });

        var ship2 = game.MyField.PutShip("�2", "�2");
        CollectionAssert.AreEqual(ship2.Decks, new Address[] { "�2", "�2" });

        var ship3 = game.MyField.PutShip("�10", "�10");
        CollectionAssert.AreEqual(ship3.Decks, new Address[] { "�10" });
    }

    [Test]
    public void ShootTest()
    {
        Game game = new();
        game.FoeField.Clear();
        var ship = game.FoeField.PutShip("�1", "�1");

        var desks = ship.Decks;
        for (int i = 0; i < ship.Decks.Length; i++)
        {
            Assert.That(ship[desks[i]] == 'H');
            var shot = game.Shoot(desks[i]);
            Assert.That(shot.Result == ship[desks[i]]);
            Assert.That(shot.Result == (i == desks.Length - 1 ? 'K' : 'I'));
        }
    }

    [Test]
    public void SaveTest()
    {
        Game game = new();
        game.MyField.PutShips();
        var fn = game.Save();
        char[,] myTestField = new char[10,10];
        char[,] foeTestField = new char[10, 10];
        Turn CurrentTurn = game.CurrentTurn;

        for (int r = 0; r < game.MyField.RowCount; r++)
            for (int c = 0; c < game.MyField.ColCount; c++)
            {
                myTestField[r, c] = game.MyField[r, c];
                foeTestField[r, c] = game.FoeField[r, c];
            }

        game.LoadFromFile(fn);

        CollectionAssert.AreEqual(myTestField, game.MyField);
        CollectionAssert.AreEqual(foeTestField, game.FoeField);
        Assert.That(CurrentTurn, Is.EqualTo(game.CurrentTurn));
    }

    [Test]
    public void IntegrationTest()
    {
        int MyWinsQbt = 0;
        int FoeWinsQnt = 0;
        Game game;

        var rnd = new Random();

        for (int i = 0; i < 300; i++)
        {
            var turn = (Turn)rnd.Next(0, 2);
            game = new Game(turn);
            game.MyField.PutShips();
            bool GameSaved = false;

            Assert.That(game.MyField.Complete);
            Assert.That(game.FoeField.Complete);

            Assert.That(game.CurrentTurn == turn);

            while (!game.Over)
            {
                game.Shoot();
                int m = game.MyField.Count(a => a == 'I' || a == 'K');
                int f = game.FoeField.Count(a => a == 'I' || a == 'K');
                if (((m == 10) || (f == 10)) && (!GameSaved))
                {
                    GameSaved = true;
                    game.LoadFromFile(game.Save());
                }
            }

            if (game.Winner == Turn.Mine)
                MyWinsQbt++;
            else
                FoeWinsQnt++;
        }
        Assert.That((double)MyWinsQbt / FoeWinsQnt >= 0.9 && (double)MyWinsQbt / FoeWinsQnt < 1.1);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavalCombat.Tests
{
    [TestFixture(TestOf = typeof(AddressRange), Description = "Game field tests")]
    internal class PlayFieldTest
    {
        [Test]
        public void PutShipByAddressTest()
        {
            PlayField field = new();

            CollectionAssert.AreEquivalent(
                new Address[] { "Б2", "Б3", "Б4", "Б5" },
                field.PutShip("Б2", "Б5").Cells);             

            CollectionAssert.AreEquivalent(
                new Address[] { "З1", "И1", "К1" },
                field.PutShip("З1", "К1").Cells);
            
            Assert.Throws<ApplicationException>(() => {
                field.PutShip("А2", "А4");
            });

            Assert.Throws<ApplicationException>(() => {
                field.PutShip("А2", "Г2");
            });

            Assert.AreEqual(7, field.Count(a => a == 'H'));
            Assert.AreEqual(100 - 7, field.Count(a => a == 'E'));
        }

        [Test]
        public void PutShipByBowAndDecksQntTest()
        {
            PlayField field = new();

            CollectionAssert.AreEquivalent(
                new Address[] { "Б2", "Б3", "Б4", "Б5" },
                field.PutShip("Б2", 4, Orientation.Vertical).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "З1", "И1", "К1" },
                field.PutShip("З1", 3, Orientation.Horizontal).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "Г2" },
                field.PutShip("Г2", 1, Orientation.Vertical).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "К10" },
                field.PutShip("К10", 1, Orientation.Horizontal).Cells);

            Assert.Throws<ApplicationException>(() => {
                field.PutShip("А2", 3, Orientation.Vertical);
            });

            Assert.Throws<ApplicationException>(() => {
                field.PutShip("А2", 4, Orientation.Horizontal);
            });

            Assert.That(field.Count(a => a == 'H'), Is.EqualTo(9));
            Assert.That(field.Count(a => a == 'E'), Is.EqualTo(100 - 9));
        }

        [Test]
        public void PutRandomShipTest()
        {
            PlayField field = new();

            Assert.That(field.PutShip(4)?.DeckCount ?? 0, Is.EqualTo(4));
            Assert.That(field.Where(a => a == 'H').Count(), Is.EqualTo(4));
            Assert.That(field.Where(a => a == 'E').Count(), Is.EqualTo(100 - 4));
        }

        [Test]
        public void PutShipsTest()
        {
            PlayField field = new();

            var ships = field.PutShips();

            Assert.That(ships.Length, Is.EqualTo(10));
            Assert.That(field.Count(a => a == 'H'), Is.EqualTo(20));
            Assert.That(field.Where(a => a == 'E').Count(), Is.EqualTo(100 - 20));
            
            
            var ShipDecks = new List<byte> { 4, 3, 3, 2, 2, 2, 1, 1, 1, 1 };
            CollectionAssert.AreEquivalent(ShipDecks.ToArray(), ships.Select(s => s.DeckCount));

            foreach(var sh in ships)
                Assert.That(sh.Neighbors.Count(n => field[n] != 'E'), Is.EqualTo(0));
        }

        [Test]
        public void TestAvailableShotsAndNextShot()
        {
            Game game = new();
            game.FoeField.Clear();
            game.FoeField.PutShip("А1", "А4");            
            game.FoeField.PutShip("Г1", "Д1");
            game.FoeField.PutShip("И3", "К3");
            game.FoeField.PutShip("Е4", "Е6");
            game.FoeField.PutShip("А10", "Г10");
            game.FoeField.PutShip("К8", "К9");
            game.FoeField.PutShip("В8", "В8");

            var af = new AddressRange("А1", "К10").Cells;
            CollectionAssert.AreEquivalent(af, game.FoeField.GetAvailableShots());
            
            game.Shoot("Б1");
            af = af.Except(new Address[] { "Б1" }).ToArray();
            CollectionAssert.AreEquivalent(af, game.FoeField.GetAvailableShots());
            Assert.That(af.Contains(game.FoeField.GetNextShot()));

            game.Shoot("А1");
            af = af.Except(new Address[] { "А1", "Б2" }).ToArray();
            CollectionAssert.AreEquivalent(af, game.FoeField.GetAvailableShots());
            Assert.That(af.Contains(game.FoeField.GetNextShot()));

            game.Shoot("А2");
            af = af.Except(new Address[] { "А2", "Б3" }).ToArray();
            CollectionAssert.AreEquivalent(af, game.FoeField.GetAvailableShots());
            Assert.That(game.FoeField.GetNextShot() == "А3");
        }
    }
}

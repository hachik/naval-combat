﻿using Newtonsoft.Json;

namespace NavalCombat;

public class Shot
{
    public Address Address { get; set; }

    public Turn Field { get; private set; }

    public char Result { get; set; }

    [JsonProperty("ReturnShots")]
    public Shot[] ReturnShots { get; set; } = new Shot[0];

    public Shot(Turn field, Address address)
    {
        Address = address;
        Field = field;
    }

    public Shot(Turn field, Address address, char result) : this(field, address)
    {
        Result = result;
    }

    public Turn? Winner { get; set; }
}

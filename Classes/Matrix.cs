﻿namespace Matrices;

public class Matrix<T> : BaseMatrix<T>, IMatrix<T>
{
    public T[,] Data { get; }

    public override T[,] ToArray()
    {
        return Data;
    }

    public override int RowCount
    {
        get
        {
            return Data?.GetLength(0) ?? 0;
        }
    }

    public override int ColCount
    {
        get
        {
            return (Data?.GetLength(0) ?? 0) == 0 ? 0 : Data.Length / Data.GetLength(0);
        }
    }

    public override T this[int r, int c]
    {
        get { return Data[r, c]; }
        set { Data[r, c] = value; }
    }

    public Matrix(T[,] a)
    {
        int rc = a?.GetLength(0) ?? 0;
        int cc = rc == 0 ? 0 : (a?.Length ?? 0) / rc;

        Data = new T[rc, cc];

        if (a != null)
            for (int r = 0; r < RowCount; r++)
                for (int c = 0; c < ColCount; c++)
                    Data[r, c] = a[r, c];
    }

    public Matrix(int RowCount, int ColCount)
    {
        Data = new T[RowCount, ColCount];
    }//: this(new T[RowCount, ColCount]) {; }

    public Matrix(int RowCount, int ColCount, T[] A) : this(RowCount, ColCount)
    {
        for (int r = 0; r < RowCount; r++)
            for (int c = 0; c < ColCount; c++)
                if (A.Length > r * ColCount + c)
                    Data[r, c] = A[r * ColCount + c];
    }

    public static implicit operator Matrix<T>(T[,] array)
    {
        return new Matrix<T>(array);
    }

    public void Fill(T value)
    {
        for (int r = 0; r < RowCount; r++)
            for (int c = 0; c < ColCount; c++)
                this[r,c] = value;
    }
}
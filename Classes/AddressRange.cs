﻿namespace NavalCombat;

public class AddressRange
{
    public Address Start;

    public Address End;

    public AddressRange(Address Start, Address End)
    {
        this.Start = Start;
        this.End = End;
    }

    public AddressRange(Address Start, byte Height, byte Width)
        : this(Start, new Address(Start.Row + Height - 1, Start.Col + Width - 1))
    { ; }

    public byte Count
    {
        get { return (byte)(Width * Height); }
    }

    public byte Width
    {
        get
        {
            return (byte)((Address.HToInt(End.Horizontal) - Address.HToInt(Start.Horizontal)) + 1);
        }
    }

    public byte Height
    {
        get
        {
            return (byte)((End.Vertical - Start.Vertical) + 1);
        }
    }

    public Address[] Cells
    {
        get
        {
            Address[] result = new Address[Count];

            for (int c = 0; c < Width; c++)
                for (int r = 0; r < Height; r++)
                    result[r * Width + c] = new Address(Start.Row + r, Start.Col + c);

            return result;
        }
    }

    public Address[] Neighbors
    {
        get
        {
            List<Address> result = new() { Capacity = Width * 2 + Height * 2 + 4 };

            for (int c = Math.Max(Start.Col - 1, 0); c <= Math.Min(End.Col + 1, 9); c++)
            {
                if (Start.Row > 0)
                    result.Add(new Address(Start.Row - 1, c));
                if (End.Row < 9)
                    result.Add(new Address(End.Row + 1, c));
            }

            for (int r = Start.Row; r <= End.Row; r++)
            {
                if (Start.Col > 0)
                    result.Add(new Address(r, Start.Col - 1));
                if (End.Col < 9)
                    result.Add(new Address(r, End.Col + 1));
            }

            return result.ToArray();
        }
    }

    public static AddressRange FromArray(Address[] a)
    {
        Address Start = new(a.Min(a => a.Row), a.Min(a => a.Col));
        Address End = new(a.Max(a => a.Row), a.Max(a => a.Col));

        var result = new AddressRange(Start, End);

        if (result.Count != a.Distinct().Count())
            throw new ArgumentException("Объединяемые области не образуют прямоугольник");
        else
            return result;
    }

    public static AddressRange Union(params AddressRange[] ranges)
    {
        Address Start = new(ranges.Min(a => a.Start.Row), ranges.Min(a => a.Start.Col));
        Address End = new(ranges.Max(a => a.End.Row), ranges.Max(a => a.End.Col));
        var result = new AddressRange(Start, End);

        if (result.Count != ranges.SelectMany(r => r.Cells).Distinct().Count())
            throw new ArgumentException("Объединяемые области не образуют прямоугольник");
        else
            return result;
    }
}
﻿using Newtonsoft.Json;
using System.Collections;

namespace Matrices;

public abstract class BaseMatrix<T> : IMatrix<T>, IEnumerable<T>
{
    public string? Name { get; set; }

    public abstract T this[int r, int c] { get; set; }

    public abstract int RowCount { get; }

    public abstract int ColCount { get; }

    public T? ElementAtOrDefault(int index)
    {
        if (index < RowCount * ColCount)
            return this[index / ColCount, index - index / ColCount * ColCount];
        else
            return default;
    }

    public virtual bool IsSymmetric
    {
        get
        {
            if (RowCount != ColCount)
                return false;

            for (int i = 0; i < RowCount; i++)
                for (int j = 0; j < ColCount; j++)
                    if (!Equals(this[i, j], this[j, i]))
                        return false;

            return true;
        }
    }

    public virtual bool IsSquare { get { return RowCount == ColCount; } }

    public BaseMatrix() { ; }

    public BaseMatrix(T[,] a)
    {
        int rc = a?.GetLength(0) ?? 0;
        int cc = rc == 0 ? 0 : (a?.Length ?? 0) / rc;

        if (a != null)
            for (int r = 0; r < rc; r++)
                for (int c = 0; c < cc; c++)
                    this[r, c] = a[r, c];
    }

    public BaseMatrix(int RowCount, int ColCount) : this(new T[RowCount, ColCount]) {; }

    public BaseMatrix(int RowCount, int ColCount, T[] A) : this(RowCount, ColCount)
    {
        for (int r = 0; r < RowCount; r++)
            for (int c = 0; c < ColCount; c++)
                if (A.Length > r * ColCount + c)
                    this[r, c] = A[r * ColCount + c];
    }

    public virtual T[,] ToArray()
    {
        var a = new T[RowCount, ColCount];
        for (int r = 0; r < RowCount; r++)
            for (int c = 0; c < ColCount; c++)
                a[r, c] = this[r, c];

        return a;
    }

    public override string ToString()
    {
        /*var sb = new StringBuilder();
        sb.Append("{");
        for (int r = 0; r < RowCount; r++)
        {
            sb.Append(" {");
            for (int c = 0; c < ColCount; c++)
            {
                sb.Append(this[r, c] + (c == ColCount - 1 ? "" : ", "));
            }
            sb.Append("} " + (r == RowCount - 1 ? "" : ", "));
        }
        sb.Append("}");

        return sb.ToString();*/
        return $"Matrix{(" " + Name + " ").Trim()}[{RowCount}x{ColCount}]";
    }

    public IEnumerator<T> GetEnumerator()
    {
        foreach (T element in ToArray()) yield return element;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public virtual string ToJsonString()
    {
        return JsonConvert.SerializeObject(ToArray());
    }

    public virtual void FromJsonString(string s)
    {
        T[,]? data = JsonConvert.DeserializeObject<T[,]>(s);

        if (data != null)
            for (int r = 0; r < RowCount; r++)
                for (int c = 0; c < ColCount; c++)
                    this[r,c] = data[r,c];
    }

    /*public static void DeserializeItem(string fileName, IFormatter formatter)
    {
        FileStream s = new FileStream(fileName, FileMode.Open);
        MyItemType t = (MyItemType)formatter.Deserialize(s);
        Console.WriteLine(t.MyProperty);
    }*/

    public virtual T[] Values
    {
        get { return this.Select(a => a).ToArray(); }
    }
}

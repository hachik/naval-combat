﻿namespace NavalCombat;

public enum MoveDirection
{
    Left,
    Right,
    Up,
    Down,
}

public struct Move
{
    public int H;
    public int V;

    public Move(int h, int v)
    {
        H = h; V = v;
    }
}

public class Address
{
    char h;
    int v;

    public static int HToInt(char h)
    {
        if (!(new int[] { 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', }).Contains(h))
            throw new ArgumentOutOfRangeException(nameof(h));
        return h - 1039 - (h == 'К' ? 1 : 0);
    }

    public static char IntToH(int i)
    {
        if (i <= 0 || i > 10)
            throw new ArgumentOutOfRangeException(nameof(i));

        return (char)(i + 1039 + (i == 10 ? 1 : 0));
    }

    public char Horizontal
    {
        get { return h; }
        private set
        {
            if (!(value >= 1040 && value <= 1050))
                throw new Exception("По горизонтали может быть от А до К");
            h = value;
        }
    }

    public int Vertical
    {
        get { return v; }
        private set
        {
            if (!(value >= 1 && value <= 10))
                throw new Exception("По вертикали может быть от 1 до 10");
            v = value;
        }
    }

    public int Row
    {
        get { return Vertical - 1; }
    }

    public int Col
    {
        get { return HToInt(Horizontal) - 1; }
    }

    public Address(char Horizontal, int Vertical)
    {
        if (Vertical < 1 || Vertical > 10)
            throw new ArgumentOutOfRangeException("По вертикали может быть от 1 до 10");
        if (Horizontal < 'А' || Horizontal > 'К' || Horizontal == 'Й')
            throw new ArgumentOutOfRangeException("По горизонтали может быть от А до К (исключая Ё и Й)");

        this.Horizontal = Horizontal;
        this.Vertical = Vertical;
    }

    public Address(int Row, int Col) : this(IntToH(Col + 1), Row + 1) {; }

    public Address(string s) : this(s[0], int.Parse(s.Substring(1))) {; }

    public Address[] GetNeibours()
    {
        List<Address> result = new() { Capacity = 8 };

        for (int i = (Row == 0 ? Row : Row - 1); i <= (Row == 9 ? Row : Row + 1); i++)
            for (int j = (Col == 0 ? Col : Col - 1); j <= (Col == 9 ? Col : Col + 1); j++)
                if (!(i == Row && j == Col))
                    result.Add(new Address(i, j));

        return result.ToArray();
    }

    public Address? Move(int H, int V)
    {
        int r = Row + V;
        int c = Col + H;
        if ((r < 0) || (r > 9) || (c < 0) || (c > 9))
            return null;

        return new Address(r, c);
    }
    public Address? Move(Move Direction)
    {
        return Move(Direction.H, Direction.V);
    }

    public Address? Move(MoveDirection Direction)
    {
        return Direction switch
        {
            MoveDirection.Left => Move(-1, 0),
            MoveDirection.Right => Move(1, 0),
            MoveDirection.Up => Move(0, -1),
            MoveDirection.Down => Move(0, 1),
            _ => throw new NotImplementedException(),
        };
    }

    public static Address[] GetBorder(Address address)
    {
        List<Address> result = new() { Capacity = 8 };

        for (int i = (address.Row == 0 ? address.Row : address.Row - 1); i <= (address.Row == 9 ? address.Row : address.Row + 1); i++)
            for (int j = (address.Col == 0 ? address.Col : address.Col - 1); j <= (address.Col == 9 ? address.Col : address.Col + 1); j++)
                if (!(i == address.Row && j == address.Col))
                    result.Add(new Address(i, j));

        return result.ToArray();
    }

    public static Address[] GetArea(Address address)
    {
        List<Address> result = new() { Capacity = 9 };
        result.Add(address);
        result.AddRange(GetBorder(address));

        return result.ToArray();
    }

    public override string ToString()
    {
        return Horizontal + "" + Vertical;
    }

    public static implicit operator string(Address adress)
    {
        return adress.ToString();
    }

    public static implicit operator Address(string s)
    {
        return new Address(s);
    }

    public static bool operator ==(Address? address1, Address? address2)
    {
        return ((address1 != null) && (address2 != null)) && (address1.ToString() == address2.ToString());
    }

    public static Move operator -(Address address1, Address address2)
    {
        return new Move(address2.Col - address1.Col, address2.Row - address1.Row);
    }

    public static bool operator !=(Address? address1, Address? address2)
    {
        return ((address1?.ToString() ?? "-") != (address2?.ToString() ?? "-"));
    }

    public override bool Equals(object? obj) => obj != null && this.ToString() == obj.ToString();

    public override int GetHashCode()
    {
        return Row * 10 + Col;
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NavalCombat;
using System.IO;
using System.Net;
using System.Reflection.Metadata.Ecma335;

namespace SeaBattle.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        public readonly Game TheGame;

        public IndexModel(ILogger<IndexModel> logger, Game theGame)
        {
            _logger = logger;
            TheGame = theGame;
        }

        public void OnGet()
        {
            if (!TheGame.Started
                    && Request.Cookies.ContainsKey("unfinished_game")
                    && System.IO.File.Exists(Request.Cookies["unfinished_game"]))
                TheGame.LoadFromFile(Request.Cookies["unfinished_game"]!);
        }

        public void OnGetReStart()
        {
            TheGame.ReStart();
        }

        public void OnGetSave()
        {
             if ((!TheGame.Over) && (TheGame.Started))
                Response.Cookies.Append("unfinished_game", TheGame.Save(), new CookieOptions { MaxAge = TimeSpan.FromDays(10) });
        }

        public ActionResult OnGetPutShips()
        {
            TheGame.MyField.Clear();
            TheGame.MyField.PutShips();
            return Content(TheGame.MyField.ToHtml());
        }

        public JsonResult OnGetNextShot()
        {
            var j = new JsonResult(TheGame.FoeField.GetNextShot().ToString());
            return j;
        }

        public JsonResult OnPostSend(string address)
        {
            var result = TheGame.Shoot(address);
            
            if (TheGame.Over)
                Response.Cookies.Delete("unfinished_game");

            return new JsonResult(result);
        }
    }
}